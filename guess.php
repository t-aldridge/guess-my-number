<?php
session_start();
if ($_COOKIE['counter'] == '0') {
    header('refresh:0;url=overlimit.html');
}
?>
<head>
<link rel="icon" href="../logo.png" type="image/png" sizes="16x16">
<title>Guess My Number</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../master.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-107294088-1"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments)};
	gtag('js', new Date());

	gtag('config', 'UA-107294088-1');
</script>
<style>
body,h1,h2,form,input,label{font-family: "Raleway", sans-serif}
body, html {height: 100%}
.bgimg {
    background: #607d8b;
    min-height: 100%;
    background-position: center;
    background-size: cover;
}
</style>
</head>
<body>

<div class="bgimg display-container animate-opacity text-white">
  <div class="display-topleft padding-large xlarge">
		<a href="../index.php"><i class="fa fa-home"></i> Go Home</a>
  </div>
  
  <div class="display-middle center">
    <form class="container" action="check.php" method="POST">
      <p>      
      <label><b>Enter a number from 1-100</b></label>
      <input class="input" name="guess" type="text"></p>
      <input type="submit" name="submit" value="Guess!" class="btn blue"></input><p></p>
			Hint:&nbsp
       <?php
       echo $_COOKIE['user_guess_status'];
       ?>
			<br><br>
        <span class="center xlarge topbar border-grey" style="padding-top:10px;">Guesses Left:
        <?php
            echo $_COOKIE['counter'];
        ?>
        </span>
    </form>

  </div>
  <div class="display-bottomleft padding-large">
    Made by Teymour Aldridge
  </div>
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
<script>
window.addEventListener("load", function(){
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#000"
    },
    "button": {
      "background": "#f1d600"
    }
  },
  "showLink": false,
  "content": {
    "message": "We use cookies to make sure that we know all your personal information and everything else there is to know about you.",
    "dismiss": "Cool!"
  }
})});
</script>

</div>
<script type="text/javascript">
window._idl = {};
_idl.variant = "modal";
(function() {
    var idl = document.createElement('script');
    idl.async = true;
    idl.src = 'https://members.internetdefenseleague.org/include/?url=' + (_idl.url || '') + '&campaign=' + (_idl.campaign || '') + '&variant=' + (_idl.variant || 'modal');
    document.getElementsByTagName('body')[0].appendChild(idl);
})();
</script>
</body>
</html>


</div>

</body>
</html>
