<?php

session_start();
$user_guess = $_POST['guess'];
// Get the number of tries it took the user to guess the number.
$attempt = 7 - intval($_COOKIE['counter']);
$counter = intval($_COOKIE['counter']);
// If the user's guess was correct set the state-cookie (the text that tells the user what they got.)
if ($user_guess == $_SESSION['my_number']) {
    $guessed = "You guessed the number after $attempt attempts. Simply guess again to restart.";
    setcookie('user_guess_status', $guessed, time() + (86400 * 7)); // 86400 = 1 day
    header('Refresh:0; url=index.php');
} elseif ($user_guess > $_SESSION['my_number']) {
    $counter -= 1;
    $guessed = 'Guess Lower';
    setcookie('user_guess_status', $guessed, time() + (86400 * 7)); // 86400 = 1 day
    setcookie('counter', $counter, time() + (86400 * 7));
    header('Refresh:0; url=guess.php');
} else {
    $counter -= 1;
    $guessed = 'Guess Higher';
    setcookie('user_guess_status', $guessed, time() + (86400 * 7)); // 86400 = 1 day
    setcookie('counter', $counter, time() + (86400 * 7));
    header('Refresh:0; url=guess.php');
}
